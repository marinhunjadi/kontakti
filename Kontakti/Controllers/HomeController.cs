﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Kontakti.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index(int offset = 0, string orderBy = "ID", string orderDirection = "ASC", string firstName = "", string lastName = "")
        {
            SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
            List<Contact> contacts = new List<Contact>();
            using (connection)
            {
                SqlCommand command = new SqlCommand(
                  "uspGetContacts",
                  connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter offsetParameter = new SqlParameter("@Offset", offset);
                SqlParameter orderByParameter = new SqlParameter("@OrderBy", orderBy);
                SqlParameter orderDirectionParameter = new SqlParameter("@OrderDirection", orderDirection);
                SqlParameter firstNameParameter = new SqlParameter("@FirstName", firstName.Trim());
                SqlParameter lastNameParameter = new SqlParameter("@LastName", lastName.Trim());
                command.Parameters.Add(offsetParameter);
                command.Parameters.Add(orderByParameter);
                command.Parameters.Add(orderDirectionParameter);
                command.Parameters.Add(firstNameParameter);
                command.Parameters.Add(lastNameParameter);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Contact contact = new Contact();
                        contact.ID = reader.GetInt32(0);
                        contact.FirstName = reader.GetString(1);
                        contact.LastName = reader.GetString(2);
                        contact.Phone = reader.GetString(3);
                        contact.Email = reader.GetString(4);
                        contact.CreatedDateTime = reader.GetDateTime(5);
                        contacts.Add(contact);
                    }
                }
                else
                {

                }
                reader.Close();
            }
            ViewBag.FirstName = firstName;
            ViewBag.LastName = lastName;
            return View(contacts.AsEnumerable<Contact>());
        }

        // GET: Home/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
            Contact contact = new Contact();
            using (connection)
            {
                SqlCommand command = new SqlCommand(
                  "uspGetContact",
                  connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter IDParameter = new SqlParameter("@ID", id);
                command.Parameters.Add(IDParameter);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    contact.ID = reader.GetInt32(0);
                    contact.FirstName = reader.GetString(1);
                    contact.LastName = reader.GetString(2);
                    contact.Phone = reader.GetString(3);
                    contact.Email = reader.GetString(4);
                    contact.CreatedDateTime = reader.GetDateTime(5);
                }
                else
                {
                    contact = null;
                }
                reader.Close();
            }

            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // GET: Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Home/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,FirstName,LastName,Phone,Email")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
                using (connection)
                {
                    SqlCommand command = new SqlCommand(
                      "uspInsertContact",
                      connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    
                    SqlParameter firstNameParameter = new SqlParameter("@FirstName", contact.FirstName);
                    SqlParameter lastNameParameter = new SqlParameter("@LastName", contact.LastName);
                    SqlParameter phoneParameter = new SqlParameter("@Phone", contact.Phone);
                    SqlParameter emailParameter = new SqlParameter("@Email", contact.Email);
                    
                    command.Parameters.Add(firstNameParameter);
                    command.Parameters.Add(lastNameParameter);
                    command.Parameters.Add(phoneParameter);
                    command.Parameters.Add(emailParameter);
                    connection.Open();

                    int id = Convert.ToInt32(command.ExecuteScalar());
                }
                return RedirectToAction("Index");
            }

            return View(contact);
        }

        // GET: Home/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
            Contact contact = new Contact();
            using (connection)
            {
                SqlCommand command = new SqlCommand(
                  "uspGetContact",
                  connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter IDParameter = new SqlParameter("@ID", id);
                command.Parameters.Add(IDParameter);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    contact.ID = reader.GetInt32(0);
                    contact.FirstName = reader.GetString(1);
                    contact.LastName = reader.GetString(2);
                    contact.Phone = reader.GetString(3);
                    contact.Email = reader.GetString(4);
                    contact.CreatedDateTime = reader.GetDateTime(5);
                }
                else
                {
                    contact = null;
                }
                reader.Close();
            }

            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Home/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,FirstName,LastName,Phone,Email")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
                using (connection)
                {
                    SqlCommand command = new SqlCommand(
                      "uspUpdateContact",
                      connection);
                    command.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter IDParameter = new SqlParameter("@ID", contact.ID);
                    SqlParameter firstNameParameter = new SqlParameter("@FirstName", contact.FirstName);
                    SqlParameter lastNameParameter = new SqlParameter("@LastName", contact.LastName);
                    SqlParameter phoneParameter = new SqlParameter("@Phone", contact.Phone);
                    SqlParameter emailParameter = new SqlParameter("@Email", contact.Email);

                    command.Parameters.Add(IDParameter);
                    command.Parameters.Add(firstNameParameter);
                    command.Parameters.Add(lastNameParameter);
                    command.Parameters.Add(phoneParameter);
                    command.Parameters.Add(emailParameter);
                    connection.Open();

                    int id = (int)command.ExecuteScalar();
                }
                return RedirectToAction("Index");
            }
            return View(contact);
        }

        // GET: Home/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
            Contact contact = new Contact();
            using (connection)
            {
                SqlCommand command = new SqlCommand(
                  "uspGetContact",
                  connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter IDParameter = new SqlParameter("@ID", id);
                command.Parameters.Add(IDParameter);
                connection.Open();

                SqlDataReader reader = command.ExecuteReader();

                if (reader.Read())
                {
                    contact.ID = reader.GetInt32(0);
                    contact.FirstName = reader.GetString(1);
                    contact.LastName = reader.GetString(2);
                    contact.Phone = reader.GetString(3);
                    contact.Email = reader.GetString(4);
                    contact.CreatedDateTime = reader.GetDateTime(5);
                }
                else
                {
                    contact = null;
                }
                reader.Close();
            }

            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: Home/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SqlConnection connection = new SqlConnection("Server=MARIN-LENOVO\\SQLEXPRESS;Database=Contacts;Trusted_Connection=True;");
            Contact contact = new Contact();
            using (connection)
            {
                SqlCommand command = new SqlCommand(
                  "uspDeleteContact",
                  connection);
                command.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter IDParameter = new SqlParameter("@ID", id);
                command.Parameters.Add(IDParameter);
                connection.Open();

                int rows = (int)command.ExecuteScalar();
            }
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult ContactUs()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}